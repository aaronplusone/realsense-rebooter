#!/bin/sh
cd realsense-docker
DOCKER_BUILDKIT=1 docker build . -f realsense-docker.dockerfile -t rs-ros
cd ../rebooter
DOCKER_BUILDKIT=1 docker build . -f rebooter.dockerfile -t rebooter
