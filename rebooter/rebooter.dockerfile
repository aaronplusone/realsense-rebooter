#syntax=docker/dockerfile:latest

FROM registry.gitlab.com/plusone-robotics/product/pickone/pickone/sources:bionic-melodic-ros

RUN apt-get update -qq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
  bc ros-melodic-rosbash ros-melodic-sensor-msgs ros-melodic-rostopic ros-melodic-rosservice ros-melodic-rosnode \
  > /dev/null \
 && apt-get autoremove -y > /dev/null \
 && rm -rf /var/lib/apt/lists/*

COPY killer.sh /killer.sh
RUN chmod +x /killer.sh

ENTRYPOINT ["sh", "-c", ". /opt/ros/*/setup.sh && /killer.sh >> log.txt"]
