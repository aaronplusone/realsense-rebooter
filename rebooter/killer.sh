#!/usr/bin/env sh

set -e

datestamp () {
  date +"%Y-%m-%d %T"
}

check_topic () {
  topic=${1:-/camera0/rgb/image}
  no_new_message_limit=${2:-5}
  recovery_service=${3:-/camera1/realsense2_camera/reset}
  stdbuf -oL rostopic hz $topic | {
    COUNTER=0
    while IFS= read -r line
    do
      # Expecting a positive rate a la:
      # average rate: 29.709
      rate="$(echo $line | awk -F 'average rate: ' '{print $2}')"
      if [ $rate ] && [ "$(echo "$rate>0.0" | bc)" -eq "1" ]; then
        echo "$(datestamp): Observed rate for $topic: $rate"
        exit 0

      # "no new messages" shows when a topic has advertised, but is currently dead
      elif [ "$line" = "no new messages" ]; then
        COUNTER=$((COUNTER-1))

        if [ $COUNTER -lt 0 ]; then
          COUNTER=$no_new_message_limit
          echo  "$(datestamp): \"No new messages\" on $topic; attempting reset"
          rosservice call $recovery_service > /dev/null
        fi
      fi
    done
  }
}

topic_delay=30
kill_delay=3

echo "$(datestamp): Awake. Sleep for $topic_delay s"
sleep $topic_delay
check_topic /camera1/color/image_raw 20 /camera1/realsense2_camera/reset
check_topic /camera2/color/image_raw 20 /camera2/realsense2_camera/reset
echo "$(datestamp): Sleeping $kill_delay s before kill"
sleep $kill_delay
echo b > /sysrq