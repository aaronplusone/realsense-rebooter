touch /home/$USER/reboot-log.txt
docker run -d --name rebooter --net host --restart unless-stopped -v /proc/sysrq-trigger:/sysrq -v /home/$USER/reboot-log.txt:/log.txt rebooter