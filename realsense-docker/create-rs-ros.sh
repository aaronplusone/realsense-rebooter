docker run -dit --name rs --device-cgroup-rule "c 81:* rmw" --device-cgroup-rule "c 189:* rmw" \
 -v /dev:/dev --net host --restart unless-stopped rs-ros \
 sh -c ". ws/install/setup.sh && roslaunch realsense2_camera rs_multiple_devices.launch"
