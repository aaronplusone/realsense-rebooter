# syntax = docker/dockerfile:latest

ARG BASE_IMAGE=ubuntu:18.04
#################################
#   Librealsense Builder Stage  #
#################################
FROM $BASE_IMAGE as librealsense-builder

ARG LIBRS_VERSION="2.50.0"
# Make sure that we have a version number of librealsense as argument
RUN test -n "$LIBRS_VERSION"

# To avoid waiting for input during package installation
ENV DEBIAN_FRONTEND=noninteractive

# Builder dependencies installation
RUN apt-get update \
    && apt-get install -qq -y --no-install-recommends \
    build-essential \
    cmake \
    git \
    libssl-dev \
    libusb-1.0-0-dev \
    pkg-config \
    libgtk-3-dev \
    libglfw3-dev \
    libgl1-mesa-dev \
    libglu1-mesa-dev \
    curl \
    python3 \
    python3-dev \
    python3-pip \
    ca-certificates \
    && rm -rf /var/lib/apt/lists/*

# Had an issue with the base image stage, so adding this
RUN python3 -m pip install pyrealsense2

# Download sources
WORKDIR /usr/src
RUN curl https://codeload.github.com/IntelRealSense/librealsense/tar.gz/refs/tags/v$LIBRS_VERSION -o librealsense.tar.gz
RUN tar -zxf librealsense.tar.gz \
    && rm librealsense.tar.gz
RUN ln -s /usr/src/librealsense-$LIBRS_VERSION /usr/src/librealsense

# Build and install
RUN cd /usr/src/librealsense \
    && mkdir build && cd build \
    && cmake \
    -DCMAKE_C_FLAGS_RELEASE="${CMAKE_C_FLAGS_RELEASE} -s" \
    -DCMAKE_CXX_FLAGS_RELEASE="${CMAKE_CXX_FLAGS_RELEASE} -s" \
    -DCMAKE_INSTALL_PREFIX=/opt/librealsense \
    -DBUILD_GRAPHICAL_EXAMPLES=OFF \
    -DBUILD_PYTHON_BINDINGS:bool=true \
    -DCMAKE_BUILD_TYPE=Release ../ \
    && make -j$(($(nproc)-1)) all \
    && make install

######################################
#   librealsense Base Image Stage    #
######################################
FROM ${BASE_IMAGE} as librealsense

# Copy binaries from builder stage
COPY --from=librealsense-builder /opt/librealsense /usr/local/
#COPY --from=librealsense-builder /usr/lib/python3/dist-packages/pyrealsense2 /usr/lib/python3/dist-packages/pyrealsense2
COPY --from=librealsense-builder /usr/local/lib/python3.6/dist-packages/pyrealsense2 /usr/lib/python3/dist-packages/pyrealsense2
COPY --from=librealsense-builder /usr/src/librealsense/config/99-realsense-libusb.rules /etc/udev/rules.d/
ENV PYTHONPATH=$PYTHONPATH:/usr/local/lib

# Install dep packages
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    libusb-1.0-0 \
    udev \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common \
    && rm -rf /var/lib/apt/lists/*

FROM librealsense as realsense-ros
ARG DEBIAN_FRONTEND=noninteractive
ENV SOURCES_PATH "/etc/apt/sources.list.d"
RUN apt-get update -qq \
 && apt-get install --no-install-recommends -qqy apt-utils apt-transport-https ca-certificates curl dirmngr gnupg lsb-release > /dev/null
RUN mkdir -p ~/.gnupg && chmod 700 ~/.gnupg

RUN FINGERPRINT="C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654" \
  && KEYRING="/usr/share/keyrings/ros-archive-keyring.gpg" \
  && KEYSERVER="hkp://keyserver.ubuntu.com:80" \
  && SIGNED_BY="signed-by=${KEYRING}" \
  && gpg --no-default-keyring --keyring $KEYRING --keyserver "${KEYSERVER}" --recv-key ${FINGERPRINT} \
  && echo "deb [${SIGNED_BY}] http://packages.ros.org/ros/ubuntu $(lsb_release -cs) main" \
  > ${SOURCES_PATH}/ros-latest.list

RUN apt-get update -qq \
 && apt-get install --no-install-recommends -y ros-melodic-ros-core python3-catkin-tools git ros-melodic-catkin build-essential python-rosdep > /dev/null

RUN mkdir -p /ws/src

WORKDIR /ws/src
RUN git clone https://github.com/IntelRealSense/realsense-ros.git \
 && git -C realsense-ros checkout `git tag | sort -V | grep -P "^2.\d+\.\d+" | tail -1`

RUN rosdep init && rosdep update --rosdistro=melodic

RUN ROS_DISTRO=melodic ROS_PYTHON_VERSION=2 rosdep install --skip-keys librealsense2 --from-paths /ws/src --ignore-src -y

WORKDIR /
RUN catkin config --workspace /ws --extend /opt/ros/melodic --install --default-install-space -DCATKIN_ENABLE_TESTING=False -DCMAKE_BUILD_TYPE=Release
COPY rs_multiple_devices.launch /ws/src/realsense-ros/realsense2_camera/launch/rs_multiple_devices.launch
RUN catkin build --workspace /ws -j16